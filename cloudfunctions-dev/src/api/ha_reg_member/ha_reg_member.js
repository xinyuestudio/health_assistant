import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	if (!event.group_id) {
		return {
			status: -1,
			msg: '团体信息有误！'
		}
	}else if (!event.person_name) {
		return {
			status: -1,
			msg: '姓名有误！'
		}
	} else if (!event.person_identity ||
		!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/
		.test(event.person_identity)) {
		return {
			status: -1,
			msg: '身份证号有误，请重新输入！'
		}
	}

	// 检查是否也许注册
	const collection1 = db.collection('ha_group_list');
	let res1 = await collection1
		.doc(
			event.group_id
		)
		.add(event);

	if (res1.data && res1.affectedDocs === 1 && res1.data[0].is_reg === 1){
	} else {
		return {
			status: -2,
			msg: "团体不存在或未开放注册！"
		}
	}

	const collection = db.collection('ha_group_person');
	let user = await collection.add(event);

	return {
		status: 0,
		data: {
			gp_id: user.id
		},
		msg: "注册成功"
	}
}

export {
	Perform as main
}
